﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.SqlClient;
using System.Windows.Forms;

namespace DBMidProject
{
    public partial class StudentGroup : Form
    {
        public StudentGroup()
        {
            InitializeComponent();
            InsertData();
        }

        private void StudentGroup_Load(object sender, EventArgs e)
        {

        }
        private void InsertData()
        {
            var con = Configuration.getInstance().getConnection();
            SqlCommand cmd = new SqlCommand("SELECT [Group].Id,[Group].Created_On from [Group]where(select COUNT(StudentId)from GroupStudent where GroupId=[Group].Id and Status=3)<=3", con);
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            DataTable dt = new DataTable();
            da.Fill(dt);
            dataGridView1.DataSource = dt;

            SqlCommand cmd2 = new SqlCommand("Select * from [Student]", con);
            SqlDataAdapter da2 = new SqlDataAdapter(cmd2);
            DataTable dt2 = new DataTable();
            da2.Fill(dt2);
            dataGridView2.DataSource = dt2;



            SqlCommand cmd3 = new SqlCommand("SELECT [Group].Id from [Group]where(select COUNT(StudentId)from GroupStudent where GroupId=[Group].Id and Status=3)<=3", con);
            SqlDataAdapter da3 = new SqlDataAdapter(cmd3);
            DataSet ds3 = new DataSet();
            da3.Fill(ds3, "Group");
            groupID.ValueMember = "Id";
            groupID.DataSource = ds3.Tables["Group"];
            groupID.SelectedIndex = -1;


            SqlCommand cmd4 = new SqlCommand("Select ID from [Student] ", con);
            SqlDataAdapter da4 = new SqlDataAdapter(cmd4);
            DataSet ds4 = new DataSet();
            da4.Fill(ds4, "Student");
            studentID.ValueMember = "Id";
            studentID.DataSource = ds4.Tables["Student"];
            studentID.SelectedIndex = -1;
        }

        private void groupID_SelectedIndexChanged(object sender, EventArgs e)
        {
            var con = Configuration.getInstance().getConnection();
            SqlCommand cmd2 = new SqlCommand("Select * from [Student] WHERE ID NOT IN (SELECT StudentID FROM GroupStudent WHERE GroupID=@GID1 and Status=3)", con);
            cmd2.Parameters.AddWithValue("@GID1", groupID.Text);
            SqlDataAdapter da2 = new SqlDataAdapter(cmd2);
            DataTable dt2 = new DataTable();
            da2.Fill(dt2);
            dataGridView2.DataSource = dt2;


            SqlCommand cmd4 = new SqlCommand("Select ID from [Student] WHERE ID NOT IN (SELECT StudentID FROM GroupStudent WHERE GroupID=@GID and Status=3)", con);
            cmd4.Parameters.AddWithValue("@GID", groupID.Text);
            SqlDataAdapter da4 = new SqlDataAdapter(cmd4);
            DataSet ds4 = new DataSet();
            da4.Fill(ds4, "Student");
            studentID.ValueMember = "Id";
            studentID.DataSource = ds4.Tables["Student"];
            studentID.SelectedIndex = -1;
        }

        private void button2_Click(object sender, EventArgs e)
        {
            if(groupID.Text!="" && studentID.Text!="")
            {
                var con = Configuration.getInstance().getConnection();
                SqlCommand cmd3 = new SqlCommand("UPDATE [GroupStudent] SET Status=@Status2 WHERE StudentId=@StudentID2 and Status=3", con);
                cmd3.Parameters.AddWithValue("@GroupID2", groupID.Text);
                cmd3.Parameters.AddWithValue("@StudentID2", studentID.Text);
                cmd3.Parameters.AddWithValue("@Status2", 4);
                cmd3.ExecuteNonQuery();


                SqlCommand cmd2 = new SqlCommand("select StudentId from GroupStudent where StudentId=@StudentID3 and GroupId=@GroupID3 and Status=@Status3", con);
                cmd2.Parameters.AddWithValue("@GroupID3", groupID.Text);
                cmd2.Parameters.AddWithValue("@StudentID3", studentID.Text);
                cmd2.Parameters.AddWithValue("@Status3", 4);
                cmd2.ExecuteNonQuery();
                var data = cmd2.ExecuteScalar();


                if (data == null)
                {
                    SqlCommand cmd = new SqlCommand("Insert into [GroupStudent] values (@GroupID,@StudentID, @Status,@AssingmentDate)", con);
                    cmd.Parameters.AddWithValue("@GroupID", groupID.Text);
                    cmd.Parameters.AddWithValue("@StudentID", studentID.Text);
                    cmd.Parameters.AddWithValue("@Status", 3);
                    cmd.Parameters.AddWithValue("@AssingmentDate", DateTime.Now);
                    cmd.ExecuteNonQuery();
                }
                else
                {
                    SqlCommand cmd = new SqlCommand("UPDATE  [GroupStudent] SET Status=@Status,AssignmentDate=@AssignmentDate where GroupID=@GroupID and StudentID=@StudentID", con);
                    cmd.Parameters.AddWithValue("@GroupID", groupID.Text);
                    cmd.Parameters.AddWithValue("@StudentID", studentID.Text);
                    cmd.Parameters.AddWithValue("@Status", 3);
                    cmd.Parameters.AddWithValue("@AssignmentDate", DateTime.Now);
                    cmd.ExecuteNonQuery();

                }




                MessageBox.Show("Group is assinged successfully");
            }
            else
            {
                MessageBox.Show("Please select the IDs");
            }
        }
    }
}
