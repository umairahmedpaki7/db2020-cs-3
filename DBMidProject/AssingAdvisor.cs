﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.SqlClient;
using System.Windows.Forms;

namespace DBMidProject
{
    public partial class AssingAdvisor : Form
    {
        public AssingAdvisor()
        {
            InitializeComponent();
            InserDataInGridAndComboBox();

        }

        private void button1_Click(object sender, EventArgs e)
        {
            if(mainAdvisorID.Text!="" && projectID.Text!="" && co_advisorID.Text != "" && industryAdvisorID.Text != "" && mainAdvisorID.Text!=co_advisorID.Text && mainAdvisorID.Text!=industryAdvisorID.Text && co_advisorID.Text!=industryAdvisorID.Text)
            {
                var con = Configuration.getInstance().getConnection();
                SqlCommand cmd = new SqlCommand("Select ID from Advisor where ID=@ID ", con);
                cmd.Parameters.AddWithValue("@ID", mainAdvisorID.Text);
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                DataTable dt = new DataTable();
                da.Fill(dt);

                SqlCommand cmd5 = new SqlCommand("Select ID from Advisor where ID=@ID3 ", con);
                cmd.Parameters.AddWithValue("@ID3", co_advisorID.Text);
                SqlDataAdapter da5 = new SqlDataAdapter(cmd5);
                DataTable dt5 = new DataTable();
                da.Fill(dt5);

                SqlCommand cmd4 = new SqlCommand("Select ID from Advisor where ID=@ID4 ", con);
                cmd.Parameters.AddWithValue("@ID4", industryAdvisorID.Text);
                SqlDataAdapter da4 = new SqlDataAdapter(cmd4);
                DataTable dt4 = new DataTable();
                da.Fill(dt4);


                SqlCommand cmd2 = new SqlCommand("Select ID from project where ID=@ID1 ", con);
                cmd2.Parameters.AddWithValue("@ID1", projectID.Text);
                SqlDataAdapter da2 = new SqlDataAdapter(cmd2);
                DataTable dt2 = new DataTable();
                da2.Fill(dt2);


                if (dt==null || dt.Rows.Count==0)
                {
                    MessageBox.Show("Advisor Id Not Aailible");
                }
                else if (dt2 == null || dt2.Rows.Count == 0)
                {
                    MessageBox.Show("Project Id Not Aailible");
                }
                else if (dt5 == null || dt5.Rows.Count == 0)
                {
                    MessageBox.Show("co_Advisor Id Not Aailible");
                }
                else if (dt4 == null || dt4.Rows.Count == 0)
                {
                    MessageBox.Show("Industry Advisor Id Not Aailible");
                }
                else
                {
                    SqlCommand cmd3 = new SqlCommand("Insert into ProjectAdvisor values (@AdvisorID, @ProjectID, @AdvisorRole, @AssignmentDate)", con);
                    cmd3.Parameters.AddWithValue("@AdvisorID", int.Parse(mainAdvisorID.Text));
                    cmd3.Parameters.AddWithValue("@ProjectID", int.Parse(projectID.Text));
                    cmd3.Parameters.AddWithValue("@AdvisorRole", 11);
                    cmd3.Parameters.AddWithValue("@AssignmentDate", DateTime.Now);
                    cmd3.ExecuteNonQuery();


                    SqlCommand cmd6 = new SqlCommand("Insert into ProjectAdvisor values (@AdvisorID, @ProjectID, @AdvisorRole, @AssignmentDate)", con);
                    cmd6.Parameters.AddWithValue("@AdvisorID", int.Parse(co_advisorID.Text));
                    cmd6.Parameters.AddWithValue("@ProjectID", int.Parse(projectID.Text));
                    cmd6.Parameters.AddWithValue("@AdvisorRole", 12);
                    cmd6.Parameters.AddWithValue("@AssignmentDate", DateTime.Now);
                    cmd6.ExecuteNonQuery();

                    SqlCommand cmd7 = new SqlCommand("Insert into ProjectAdvisor values (@AdvisorID, @ProjectID, @AdvisorRole, @AssignmentDate)", con);
                    cmd7.Parameters.AddWithValue("@AdvisorID", int.Parse(industryAdvisorID.Text));
                    cmd7.Parameters.AddWithValue("@ProjectID", int.Parse(projectID.Text));
                    cmd7.Parameters.AddWithValue("@AdvisorRole", 14);
                    cmd7.Parameters.AddWithValue("@AssignmentDate", DateTime.Now);
                    cmd7.ExecuteNonQuery();


                    MessageBox.Show("Assigned successfully");
                }
            }
            else
            {
                MessageBox.Show("Select IDs Properly");
            }
            var con1 = Configuration.getInstance().getConnection();
            SqlCommand cmd8 = new SqlCommand("Select * from ProjectAdvisor ", con1);
            SqlDataAdapter da8 = new SqlDataAdapter(cmd8);
            DataTable dt8 = new DataTable();
            da8.Fill(dt8);
            dataGridView3.DataSource = dt8;
        }
        public void InserDataInGridAndComboBox()
        {
            var con = Configuration.getInstance().getConnection();
            SqlCommand cmd = new SqlCommand("SELECT Advisor.Id,Value as Designation  from Advisor,Lookup  where Designation=Lookup.Id and Lookup.Category='DESIGNATION'", con);
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            DataTable dt = new DataTable();
            da.Fill(dt);
            dataGridView1.DataSource = dt;

            SqlCommand cmd2 = new SqlCommand("Select * from project WHERE ID NOT IN (SELECT [ProjectID] FROM [ProjectAdvisor])", con);
            SqlDataAdapter da2 = new SqlDataAdapter(cmd2);
            DataTable dt2 = new DataTable();
            da2.Fill(dt2);
            dataGridView2.DataSource = dt2;

            var con1 = Configuration.getInstance().getConnection();
            SqlCommand cmd8 = new SqlCommand("SELECT AdvisorId,ProjectId,Value AS Role,AssignmentDate from ProjectAdvisor,Lookup where AdvisorRole=id order by ProjectId", con1);
            SqlDataAdapter da8 = new SqlDataAdapter(cmd8);
            DataTable dt8 = new DataTable();
            da8.Fill(dt8);
            dataGridView3.DataSource = dt8;

            SqlCommand cmd9 = new SqlCommand("SELECT ID FROM Advisor", con);
            SqlDataAdapter da3 = new SqlDataAdapter(cmd9);
            DataSet ds3 = new DataSet();
            da3.Fill(ds3, "Group");
            mainAdvisorID.ValueMember = "ID";
            mainAdvisorID.DataSource = ds3.Tables["Group"];
            mainAdvisorID.SelectedIndex = -1;

            SqlCommand cmd10 = new SqlCommand("SELECT ID FROM Advisor", con);
            SqlDataAdapter da10 = new SqlDataAdapter(cmd10);
            DataSet ds10 = new DataSet();
            da10.Fill(ds10, "Group");
            co_advisorID.ValueMember = "ID";
            co_advisorID.DataSource = ds10.Tables["Group"];
            co_advisorID.SelectedIndex = -1;


            SqlCommand cmd11 = new SqlCommand("SELECT ID FROM Advisor", con);
            SqlDataAdapter da11 = new SqlDataAdapter(cmd11);
            DataSet ds11 = new DataSet();
            da11.Fill(ds11, "Group");
            industryAdvisorID.ValueMember = "ID";
            industryAdvisorID.DataSource = ds11.Tables["Group"];
            industryAdvisorID.SelectedIndex = -1;


            SqlCommand cmd12 = new SqlCommand("Select ID from project WHERE ID NOT IN (SELECT [ProjectID] FROM [ProjectAdvisor])", con);
            SqlDataAdapter da12 = new SqlDataAdapter(cmd12);
            DataSet ds12 = new DataSet();
            da12.Fill(ds12, "Group");
            projectID.ValueMember = "ID";
            projectID.DataSource = ds12.Tables["Group"];
            projectID.SelectedIndex = -1;
        }
    }
}
