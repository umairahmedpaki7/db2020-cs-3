﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.SqlClient;
using System.Windows.Forms;
using System.Text.RegularExpressions;

namespace DBMidProject
{
    public partial class Form2 : Form
    {
        public Form2()
        {
            InitializeComponent();
            warn.Hide();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            try
            {
                if (registrationNumber.Text != "" && firstName.Text != "" && lastName.Text != "" && contact.Text != "" && email.Text != "" && dateOfBirth != null && gender.Text != "")
                {
                    new System.Net.Mail.MailAddress(email.Text);
                    Regex ex = new Regex("^(?:\\d{2}-\\d{3}-\\d{3}-\\d{3}|\\d{11})$");
                    if (ex.IsMatch(contact.Text))
                    {
                        if (Regex.IsMatch(registrationNumber.Text, @"^[2]{1}[0]{1}[0-9]{2}-[A-Z]{2}-[1-9]\d*$"))
                        {
                            var con = Configuration.getInstance().getConnection();

                            SqlCommand getrg = new SqlCommand("select RegistrationNo from Student where RegistrationNo=@regno",con);
                            getrg.Parameters.AddWithValue("regno", registrationNumber.Text);
                            getrg.ExecuteNonQuery();
                            
                            var re = getrg.ExecuteScalar();
                            if (re == null)
                            { 

                            SqlCommand cmd2 = new SqlCommand("Select ID from Lookup where Category='GENDER' AND Value=@value", con);
                            cmd2.Parameters.AddWithValue("Value", gender.Text);
                            cmd2.ExecuteNonQuery();

                            SqlCommand cmd = new SqlCommand("Insert into Person values (@FirstName, @LastName,@Contact,@Email,@DateOfBirth,@Gender)", con);
                            cmd.Parameters.AddWithValue("@FirstName", firstName.Text);
                            cmd.Parameters.AddWithValue("@LastName", lastName.Text);
                            cmd.Parameters.AddWithValue("@Contact", contact.Text);
                            cmd.Parameters.AddWithValue("@Email", email.Text);
                            cmd.Parameters.AddWithValue("@DateOfBirth", dateOfBirth.Value);
                            cmd.Parameters.AddWithValue("@Gender", int.Parse(cmd2.ExecuteScalar().ToString()));
                            cmd.ExecuteNonQuery();
                            SqlCommand cmb = new SqlCommand("INSERT INTO Student VALUES (@Id, @RegistrationNo)", con);
                            SqlCommand getID = new SqlCommand("SELECT MAX(Id) FROM Person", con);

                            getID.ExecuteNonQuery();
                            cmb.Parameters.AddWithValue("@Id", int.Parse(getID.ExecuteScalar().ToString()));
                            cmb.Parameters.AddWithValue("@RegistrationNo", registrationNumber.Text);
                            cmb.ExecuteNonQuery();
                            MessageBox.Show("Data is added successfully");
                            }
                            else
                            {
                                MessageBox.Show("Student with this RegistrationNo is already register");
                            }
                        }
                        else
                        {
                            MessageBox.Show("Please correct the format of the Registration Number");
                        }
                    }
                    else
                    {
                        MessageBox.Show("Invalid Phone Number");
                    }
                }
                else
                {
                    warn.Show();
                }
            }
            catch (ArgumentException)
            {
                //textBox is empty
            }
            catch (FormatException)
            {
                MessageBox.Show("No valid mail address");
            }

        }

        private void Form2_Load(object sender, EventArgs e)
        {

        }
    }
}
