﻿
namespace DBMidProject
{
    partial class AddAdvisor
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.warn = new System.Windows.Forms.Label();
            this.button1 = new System.Windows.Forms.Button();
            this.salary = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.designation = new System.Windows.Forms.ComboBox();
            this.SuspendLayout();
            // 
            // warn
            // 
            this.warn.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.warn.AutoSize = true;
            this.warn.ForeColor = System.Drawing.Color.Red;
            this.warn.Location = new System.Drawing.Point(271, 212);
            this.warn.Name = "warn";
            this.warn.Size = new System.Drawing.Size(263, 17);
            this.warn.TabIndex = 33;
            this.warn.Text = "Please enter the value of the above field";
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(365, 301);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(75, 23);
            this.button1.TabIndex = 32;
            this.button1.Text = "INSERT";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // salary
            // 
            this.salary.Location = new System.Drawing.Point(589, 92);
            this.salary.Name = "salary";
            this.salary.Size = new System.Drawing.Size(180, 22);
            this.salary.TabIndex = 31;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(486, 95);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(48, 17);
            this.label5.TabIndex = 29;
            this.label5.Text = "Salary";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(32, 92);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(83, 17);
            this.label1.TabIndex = 28;
            this.label1.Text = "Designation";
            // 
            // designation
            // 
            this.designation.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.designation.FormattingEnabled = true;
            this.designation.Location = new System.Drawing.Point(134, 89);
            this.designation.Name = "designation";
            this.designation.Size = new System.Drawing.Size(121, 24);
            this.designation.TabIndex = 34;
            // 
            // AddAdvisor
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.White;
            this.ClientSize = new System.Drawing.Size(800, 450);
            this.Controls.Add(this.designation);
            this.Controls.Add(this.warn);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.salary);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.label1);
            this.Name = "AddAdvisor";
            this.Text = "AddAdvisor";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label warn;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.TextBox salary;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.ComboBox designation;
    }
}