﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace DBMidProject
{
    public partial class Form1 : Form
    {
        Form activeForm;
        public Form1()
        {
            InitializeComponent();
            HideSubMenu();
        }
        private void HideSubMenu()
        {
            addPanel.Visible = false;
            advisorSubMenu.Visible = false;
            ProjectSubmenu.Visible = false;
            evaluationPane.Visible = false;
        }
        private void showsubmenu(Panel submenu)
        {

            if (submenu.Visible == false)
            {
                HideSubMenu();
                submenu.Visible = true;
            }
            else
            {
                submenu.Visible = false;
            }
        }
        private void loadChild(Form childForm)
        {
            if (activeForm != null)
            {
                activeForm.Close();
            }
            activeForm = childForm;
            childForm.TopLevel = false;
            childForm.FormBorderStyle = FormBorderStyle.None;
            childForm.Dock = DockStyle.Fill;
            childFormPanel.Controls.Add(childForm);
            childFormPanel.Tag = childForm;
            childForm.BringToFront();
            childForm.Show();
        }

        private void button2_Click(object sender, EventArgs e)
        {

            loadChild(new Form2());
        }

        private void button1_Click(object sender, EventArgs e)
        {
            showsubmenu(addPanel);
        }

        private void ShowData_Click(object sender, EventArgs e)
        {
            loadChild(new ShowStudentData());
        }

        private void button3_Click(object sender, EventArgs e)
        {
            loadChild(new UpdateDeleteStudent());
        }

        private void button4_Click(object sender, EventArgs e)
        {
            showsubmenu(advisorSubMenu);
        }

        private void button5_Click(object sender, EventArgs e)
        {
            loadChild(new AddAdvisor());
        }

        private void button6_Click(object sender, EventArgs e)
        {
            loadChild(new ShowAdvisor());
        }

        private void button7_Click(object sender, EventArgs e)
        {
            loadChild(new UpdateDeleteAdvisor());
        }

        private void button8_Click(object sender, EventArgs e)
        {
            showsubmenu(ProjectSubmenu);
        }

        private void button11_Click(object sender, EventArgs e)
        {
            loadChild(new AddProject());
        }

        private void button10_Click(object sender, EventArgs e)
        {
            loadChild(new ShowProject());
        }

        private void button9_Click(object sender, EventArgs e)
        {
            loadChild(new DeleteProject());
        }

        private void button12_Click(object sender, EventArgs e)
        {
            loadChild(new AssingAdvisor());
        }

        private void button13_Click(object sender, EventArgs e)
        {
            loadChild(new GroupProject());
        }

        private void button14_Click(object sender, EventArgs e)
        {
            loadChild(new StudentGroup());
        }

        private void EVALUATION_Click(object sender, EventArgs e)
        {
            showsubmenu(evaluationPane);
        }

        private void button15_Click(object sender, EventArgs e)
        {
            loadChild(new Evaluation());
        }

        private void button16_Click(object sender, EventArgs e)
        {
            loadChild(new ShowEvaluation());
        }

        private void button17_Click(object sender, EventArgs e)
        {
            loadChild(new UpdateEvaluation());
        }

        private void button18_Click(object sender, EventArgs e)
        {
            loadChild(new GroupWiseEvaluation());
        }

        private void button19_Click(object sender, EventArgs e)
        {
            loadChild(new createpdf());
        }
    }

}
