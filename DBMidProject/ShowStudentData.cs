﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.SqlClient;
using System.Windows.Forms;

namespace DBMidProject
{
    public partial class ShowStudentData : Form
    {
        public ShowStudentData()
        {
            InitializeComponent();
            var con = Configuration.getInstance().getConnection();
            SqlCommand cmd = new SqlCommand("Select student.ID,RegistrationNo,FirstName,LastName,Contact,Email,DateOfBirth,Lookup.Value AS Gender from Student,Person,Lookup where student.ID=person.ID  and Lookup.Category='GENDER' and Person.Gender=Lookup.Id", con);
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            DataTable dt = new DataTable();
            da.Fill(dt);
            dataGridView1.DataSource = dt;
        }
    }
}
