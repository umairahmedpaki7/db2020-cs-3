﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.SqlClient;
using System.Windows.Forms;

namespace DBMidProject
{
    public partial class GroupProject : Form
    {
        public GroupProject()
        {
            InitializeComponent();
            InsertDataInGridsAndComboBox();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            var con = Configuration.getInstance().getConnection();
            SqlCommand cmd = new SqlCommand("Insert into [Group] values (@Created_On)", con);
            cmd.Parameters.AddWithValue("@Created_On", DateTime.Now);
            cmd.ExecuteNonQuery();
            var con2 = Configuration.getInstance().getConnection();
            SqlCommand cmd2 = new SqlCommand("Select * from [Group]", con);
            SqlDataAdapter da = new SqlDataAdapter(cmd2);
            DataTable dt = new DataTable();
            da.Fill(dt);
            dataGridView1.DataSource = dt;
            MessageBox.Show("Group is created successfully");
        }

        private void button2_Click(object sender, EventArgs e)
        {
            if(groupID.Text!=""&&projectID.Text!="")
            {
                var con = Configuration.getInstance().getConnection();
                SqlCommand cmd = new SqlCommand("Insert into [GroupProject] values (@ProjectID,@GroupID, @AssingmentDate)", con);
                cmd.Parameters.AddWithValue("@ProjectID",int.Parse( projectID.Text));
                cmd.Parameters.AddWithValue("@GroupID", int.Parse(groupID.Text));
                cmd.Parameters.AddWithValue("@AssingmentDate", DateTime.Now);
                cmd.ExecuteNonQuery();
                //Update the grid and combo box
                InsertDataInGridsAndComboBox();
                //******//
                MessageBox.Show("Project Group is added successfully");
                
            }
            else
            {
                MessageBox.Show("Please select the IDs");
            }
        }
        private void InsertDataInGridsAndComboBox()
        {
            var con = Configuration.getInstance().getConnection();
            SqlCommand cmd = new SqlCommand("Select * from [Group] WHERE ID NOT IN (SELECT [GroupID] FROM [GroupProject])", con);
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            DataTable dt = new DataTable();
            da.Fill(dt);
            dataGridView1.DataSource = dt;

            SqlCommand cmd2 = new SqlCommand("Select * from project WHERE ID NOT IN(SELECT[ProjectID] FROM[GroupProject]) and ID  IN(SELECT ProjectId from ProjectAdvisor )", con);
            SqlDataAdapter da2 = new SqlDataAdapter(cmd2);
            DataTable dt2 = new DataTable();
            da2.Fill(dt2);
            dataGridView2.DataSource = dt2;

            SqlCommand cmd3 = new SqlCommand("Select ID from [Group] WHERE ID NOT IN (SELECT [GroupID] FROM [GroupProject])", con);
            SqlDataAdapter da3 = new SqlDataAdapter(cmd3);
            DataSet ds3 = new DataSet();
            da3.Fill(ds3, "Group");
            groupID.ValueMember = "Id";
            groupID.DataSource = ds3.Tables["Group"];
            groupID.SelectedIndex = -1;


            var con1 = Configuration.getInstance().getConnection();
            SqlCommand cmd4 = new SqlCommand("Select ID from project WHERE ID NOT IN(SELECT[ProjectID] FROM[GroupProject]) and ID  IN(SELECT ProjectId from ProjectAdvisor )", con1);
            SqlDataAdapter da4 = new SqlDataAdapter(cmd4);
            DataSet ds4 = new DataSet();
            da4.Fill(ds4, "project");
            projectID.ValueMember = "Id";
            projectID.DataSource = ds4.Tables["project"];
            projectID.SelectedIndex = -1;
        }
    }
}
