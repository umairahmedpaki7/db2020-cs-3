﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.SqlClient;
using System.Windows.Forms;
using System.Text.RegularExpressions;

namespace DBMidProject
{
    public partial class UpdateDeleteStudent : Form
    {
        public UpdateDeleteStudent()
        {
            InitializeComponent();
            var con = Configuration.getInstance().getConnection();
            SqlCommand cmd = new SqlCommand("SELECT Distinct(CONCAT(RegistrationNo,'>',Student.Id)) as reg FROM Person,Student", con);
            SqlDataAdapter da3 = new SqlDataAdapter(cmd);
            DataSet ds3 = new DataSet();
            da3.Fill(ds3, "Group");
            registrationNumber.ValueMember = "reg";
            registrationNumber.DataSource = ds3.Tables["Group"];
            registrationNumber.SelectedIndex = -1;
        }

        private void button1_Click(object sender, EventArgs e)
        {

        }
        Regex contact = new Regex("^[0-9]{11}");
        Regex email = new Regex("[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*@(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?");
        private void button2_Click(object sender, EventArgs e)
        {
            if (dataGridView1.Rows.Count > 0)
            {
                if (contact.IsMatch(dataGridView1.Rows[dataGridView1.CurrentRow.Index].Cells[4].FormattedValue.ToString()) && email.IsMatch(dataGridView1.Rows[dataGridView1.CurrentRow.Index].Cells[5].FormattedValue.ToString()))
                {
                    if (dataGridView1.Rows[dataGridView1.CurrentRow.Index].Cells[7].FormattedValue.ToString() == "Male" || dataGridView1.Rows[dataGridView1.CurrentRow.Index].Cells[7].FormattedValue.ToString() == "Female")
                    {
                        if (dataGridView1.Rows[dataGridView1.CurrentRow.Index].Cells[2].FormattedValue.ToString() != "")
                        {

                            var con = Configuration.getInstance().getConnection();
                            SqlCommand cmd2 = new SqlCommand("SELECT ID  from Lookup where Category='GENDER' and Value=@value", con);
                            cmd2.Parameters.AddWithValue("@value", dataGridView1.Rows[dataGridView1.CurrentRow.Index].Cells[7].FormattedValue.ToString());
                            cmd2.ExecuteNonQuery();

                            SqlCommand cmd = new SqlCommand("UPDATE person SET FirstName=@FirstName,LastName=@LastName,Contact=@Contact,Email=@Email,DateOfBirth=@DateOfBirth,Gender=@Gender WHERE ID=@ID", con);
                            cmd.Parameters.AddWithValue("@ID", dataGridView1.Rows[dataGridView1.CurrentRow.Index].Cells[0].FormattedValue.ToString());
                            cmd.Parameters.AddWithValue("@FirstName", dataGridView1.Rows[dataGridView1.CurrentRow.Index].Cells[2].FormattedValue.ToString());
                            cmd.Parameters.AddWithValue("@LastName", dataGridView1.Rows[dataGridView1.CurrentRow.Index].Cells[3].FormattedValue.ToString());
                            cmd.Parameters.AddWithValue("@Contact", dataGridView1.Rows[dataGridView1.CurrentRow.Index].Cells[4].FormattedValue.ToString());
                            cmd.Parameters.AddWithValue("@Email", dataGridView1.Rows[dataGridView1.CurrentRow.Index].Cells[5].FormattedValue.ToString());
                            cmd.Parameters.AddWithValue("@DateOfBirth", DateTime.Parse(dataGridView1.Rows[dataGridView1.CurrentRow.Index].Cells[6].FormattedValue.ToString()));
                            cmd.Parameters.AddWithValue("@Gender", int.Parse(cmd2.ExecuteScalar().ToString()));
                            cmd.ExecuteNonQuery();
                            SqlCommand cmb = new SqlCommand("UPDATE Student SET RegistrationNo=@RegistrationNo WHERE ID=@ID", con);
                            cmb.Parameters.AddWithValue("@ID", dataGridView1.Rows[dataGridView1.CurrentRow.Index].Cells[0].FormattedValue.ToString());
                            cmb.Parameters.AddWithValue("@RegistrationNo", dataGridView1.Rows[dataGridView1.CurrentRow.Index].Cells[1].FormattedValue.ToString());
                            cmb.ExecuteNonQuery();
                            MessageBox.Show("Data is updated successfully");

                            SqlCommand cmd4 = new SqlCommand("SELECT Distinct(CONCAT(RegistrationNo,'>',Student.Id)) as reg FROM Person,Student", con);
                            SqlDataAdapter da3 = new SqlDataAdapter(cmd4);
                            DataSet ds3 = new DataSet();
                            da3.Fill(ds3, "Group");
                            registrationNumber.ValueMember = "reg";
                            registrationNumber.DataSource = ds3.Tables["Group"];
                            registrationNumber.SelectedIndex = -1;
                        }
                        else
                        {
                            MessageBox.Show("First name can not be null");
                        }
                    }
                    else
                    {
                        MessageBox.Show("Please correct the gender format");
                    }
                }
                else
                {
                    MessageBox.Show("Please correct the email or contact number format");
                }
            }
            else
            {
                MessageBox.Show("Please select registration number");
            }

        }
        
        private void button3_Click(object sender, EventArgs e)
        {
            if (dataGridView1.Rows.Count > 1)
            {
                var con = Configuration.getInstance().getConnection();
                SqlCommand cmd3 = new SqlCommand("DELETE FROM GroupStudent WHERE StudentID=@ID", con);
                cmd3.Parameters.AddWithValue("@ID", dataGridView1.Rows[dataGridView1.CurrentRow.Index].Cells[0].FormattedValue.ToString());
                cmd3.ExecuteNonQuery();

                SqlCommand cmd = new SqlCommand("DELETE FROM Student WHERE ID=@ID", con);
                cmd.Parameters.AddWithValue("@ID", dataGridView1.Rows[dataGridView1.CurrentRow.Index].Cells[0].FormattedValue.ToString());
                cmd.ExecuteNonQuery();
                SqlCommand cmb = new SqlCommand("DELETE FROM person WHERE ID=@ID", con);
                cmb.Parameters.AddWithValue("@ID", dataGridView1.Rows[dataGridView1.CurrentRow.Index].Cells[0].FormattedValue.ToString());
                cmb.ExecuteNonQuery();

                SqlCommand cmd2 = new SqlCommand("SELECT Distinct(CONCAT(RegistrationNo,'>',Student.Id)) as reg FROM Person,Student", con);
                SqlDataAdapter da3 = new SqlDataAdapter(cmd2);
                DataSet ds3 = new DataSet();
                da3.Fill(ds3, "Group");
                registrationNumber.ValueMember = "reg";
                registrationNumber.DataSource = ds3.Tables["Group"];
                registrationNumber.SelectedIndex = -1;

                MessageBox.Show("Successfully Deleted");
            }
            else
            {
                MessageBox.Show("Please select registration number");
            }
        }

        private void registrationNumber_TextChanged(object sender, EventArgs e)
        {

        }

        private void dataGridView1_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {

        }

        private void label1_Click(object sender, EventArgs e)
        {

        }

        private void registrationNumber_SelectedIndexChanged(object sender, EventArgs e)
        {
        }

        private void button1_Click_1(object sender, EventArgs e)
        {
            if (registrationNumber.Text != "")
            {
                var con = Configuration.getInstance().getConnection();
                string[] arr = registrationNumber.Text.Split('>');
                SqlCommand cmd = new SqlCommand("Select student.ID,RegistrationNo,FirstName,LastName,Contact,Email,DateOfBirth,Lookup.Value AS Gender from Student,Person,Lookup where student.ID=person.ID and Student.ID=@RegistrationNumber and Lookup.Category='GENDER' and Person.Gender=Lookup.Id", con);
                cmd.Parameters.AddWithValue("@RegistrationNumber", arr[1]);
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                DataTable dt = new DataTable();
                da.Fill(dt);
                dataGridView1.DataSource = dt;
                dataGridView1.Columns["ID"].ReadOnly = true;
            }
            else
            {
                MessageBox.Show("Please select the registration number");
            }
        }

        private void dataGridView1_DataError(object sender, DataGridViewDataErrorEventArgs e)
        {
            MessageBox.Show("Please correct the form of the last updated cell");
        }
    }
}
