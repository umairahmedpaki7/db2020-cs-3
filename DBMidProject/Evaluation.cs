﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.SqlClient;
using System.Windows.Forms;
using System.Text.RegularExpressions;

namespace DBMidProject
{
    public partial class Evaluation : Form
    {
        public Evaluation()
        {
            InitializeComponent();
        }
        Regex ex = new Regex("^[0-9]*$");
        private void button1_Click(object sender, EventArgs e)
        {
            
            if (name.Text != "" && totalMarks.Text != "" && totalWeitage.Text != "")
            {
                if (ex.IsMatch(totalMarks.Text)  && ex.IsMatch(totalWeitage.Text))
                {

                    var con = Configuration.getInstance().getConnection();
                    SqlCommand cmd = new SqlCommand("Insert into Evaluation values (@Name,@TotalMarks, @TotalWeitage)", con);
                    cmd.Parameters.AddWithValue("@Name", name.Text);
                    cmd.Parameters.AddWithValue("@TotalMarks", totalMarks.Text);
                    cmd.Parameters.AddWithValue("@TotalWeitage", totalWeitage.Text);
                    cmd.ExecuteNonQuery();
                    MessageBox.Show("Evaluation is added successfully");
                }
                else
                {
                    MessageBox.Show("Please enter the weightage and marks in the correct format");
                }
            }
            else
            {
                MessageBox.Show("Please fill the input fields");
            }
        }
    }
}
