﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using iTextSharp.text;
using iTextSharp.text.pdf;
using System.IO;
using System.Data.SqlClient;
using System.Text;
using iTextSharp.text.html.simpleparser;
using iTextSharp.text.html;
using System.Drawing.Printing;



namespace DBMidProject
{
    public partial class createpdf : Form
    {
        public createpdf()
        {
            InitializeComponent();
            dataGridView1.Hide();
            select.Hide();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            if (comboBox1.Text != "")
            {
                string heading = "";
                Document dc = new Document();
                if (comboBox1.Text == "Report 1")
                {
                    PdfWriter.GetInstance(dc, new FileStream("D:/projectpdf/Report1.pdf", FileMode.Create));
                    heading = "Report of projects along with advisory board and students";
                }
                else if(comboBox1.Text=="Report 2")
                {
                    PdfWriter.GetInstance(dc, new FileStream("D:/projectpdf/Report2.pdf", FileMode.Create));
                    heading = "Report of projects along with evaluation of each students";

                }
                else if(comboBox1.Text=="Report 3")
                {
                    PdfWriter.GetInstance(dc, new FileStream("D:/projectpdf/Report3.pdf", FileMode.Create));
                    heading = "Report of the result of the each student along with percentage";
                }
                else if(comboBox1.Text=="Report 4")
                {
                    PdfWriter.GetInstance(dc, new FileStream("D:/projectpdf/Report4.pdf", FileMode.Create));
                    heading = "Report of the result of each group along with advisor role and ID";
                }
                else if(comboBox1.Text=="Report 5")
                {
                    PdfWriter.GetInstance(dc, new FileStream("D:/projectpdf/Report5.pdf", FileMode.Create));
                    heading = "Report of each student along with project working on it";
                }
                dc.Open();
                string data = "University of Engineering and Technology,Lahore";
                var p1 = new Paragraph(data, FontFactory.GetFont("Arial", 19, iTextSharp.text.Font.BOLD));
                p1.Alignment = Element.ALIGN_CENTER;

                var p2 = new Paragraph(heading, FontFactory.GetFont("Arial", 14));
                p2.Alignment = Element.ALIGN_CENTER;

                var p3 = new Paragraph(DateTime.Now.ToLongDateString().ToString()+" "+ DateTime.Now.ToLongTimeString().ToString());
                p3.Alignment = Element.ALIGN_CENTER;
                p3.SpacingAfter = 20f;

                dc.Add(p1);
                dc.Add(p2);
                dc.Add(p3);

                PdfPTable table = new PdfPTable(dataGridView1.Columns.Count);

                

                table.DefaultCell.HorizontalAlignment = Element.ALIGN_CENTER;
                table.DefaultCell.VerticalAlignment = Element.ALIGN_CENTER;
                table.DefaultCell.Padding = 6;
                table.DefaultCell.PaddingTop = 10;
                
                table.WidthPercentage = 100;
                for (int j = 0; j < dataGridView1.Columns.Count; j++)
                {
                    table.AddCell(new Phrase(dataGridView1.Columns[j].HeaderText, FontFactory.GetFont("Arial", 10, iTextSharp.text.Font.BOLD)));
                }
                for (int i = 0; i < dataGridView1.Rows.Count; i++)
                {
                    table.WidthPercentage = 100;
                    for (int j = 0; j < dataGridView1.Columns.Count; j++)
                    {
                        if (dataGridView1[j, i].Value != null)
                        {
                            table.AddCell(new Phrase(dataGridView1[j, i].Value.ToString(), FontFactory.GetFont("Arial", 10, BaseColor.BLACK)));
                        }
                    }
                }
                dc.Add(table);

                string signature = "Signature of ChairPerson____________________";
                var p4 = new Paragraph(signature, FontFactory.GetFont("Arial", 14));
                p4.Alignment = Element.ALIGN_RIGHT;
                p4.SpacingBefore = 20f;
                dc.Add(p4);



                dc.Close();
                MessageBox.Show("Your PDF report is generated successfully");
            }
            else
            {
                select.Show();
            }
            
        }

        private void comboBox1_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (comboBox1.Text == "Report 1")
            {
                var con = Configuration.getInstance().getConnection();
                SqlCommand cmd = new SqlCommand(@"select A.ProjectID,A.AdvisorID,B.[Registration Number]
                                            from
                                            /**A**/
                                            (select Project.Id as ProjectID, advisors.Id as AdvisorID
                                            from Project
                                            left join
                                            (select ProjectAdvisor.ProjectId, Advisor.Id
                                            from Advisor
                                            join ProjectAdvisor
                                            on ProjectAdvisor.AdvisorId = Advisor.Id
                                            group by ProjectAdvisor.ProjectId, Advisor.Id) as advisors
                                            on Project.Id = advisors.ProjectId) as A
                                            /**B**/
                                            full join
                                            (select GroupProject.ProjectId, students.[Registration Number]
                                            from GroupProject
                                            left join
                                            (select GroupStudent.GroupId, Student.RegistrationNo as [Registration Number]
                                            from GroupStudent, Student
                                            where GroupStudent.StudentId = Student.Id and GroupStudent.Status = 3) as students
                                            on GroupProject.GroupId = students.GroupId) as B
                                            /**/
                                            on A.ProjectID = B.ProjectId", con);
                cmd.ExecuteNonQuery();
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                DataTable dt = new DataTable();
                da.Fill(dt);
                dataGridView1.DataSource = dt;
            }
            else if(comboBox1.Text=="Report 2")
            {
                var con = Configuration.getInstance().getConnection();
                SqlCommand cmd = new SqlCommand(@"select student.RegistrationNo,student.EvaluationID,Project.Id as ProjectID,student.ObtainedMarks,student.TotalMarks
                                                from
                                                (select Student.RegistrationNo,GroupEvaluation.GroupId,Evaluation.Id as EvaluationID,ObtainedMarks,Evaluation.TotalMarks
                                                from GroupEvaluation,GroupStudent,Student,Evaluation
                                                where GroupEvaluation.GroupId=GroupStudent.GroupId and Student.Id=GroupStudent.StudentId and 
                                                GroupStudent.Status=3 and Evaluation.Id=GroupEvaluation.EvaluationId) as student
                                                full join GroupProject
                                                on student.GroupId=GroupProject.GroupId
                                                full join Project
                                                on GroupProject.ProjectId=Project.Id
                                                where student.RegistrationNo is not null", con);
                cmd.ExecuteNonQuery();
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                DataTable dt = new DataTable();
                da.Fill(dt);
                dataGridView1.DataSource = dt;
            }
            else if(comboBox1.Text=="Report 3")
            {
                var con = Configuration.getInstance().getConnection();
                SqlCommand cmd = new SqlCommand(@"select Student.RegistrationNo,stid.ObtainMarks,stid.TotaolMarks,stid.Perentage
                                                from
                                                (select GroupStudent.StudentId,mrk.ObtainMarks,mrk.TotaolMarks,mrk.Perentage
                                                from
                                                (select GroupId,sum(ObtainedMarks) as ObtainMarks,SUM(Evaluation.TotalMarks) as TotaolMarks,concat(cast(cast(cast(sum(ObtainedMarks) as float)/cast(SUM(Evaluation.TotalMarks) as float)*100 as float) as varchar),'%')  as Perentage
                                                from GroupEvaluation,Evaluation
                                                where GroupEvaluation.EvaluationId=Evaluation.Id
                                                group by GroupId) as mrk
                                                join GroupStudent
                                                on mrk.GroupId=GroupStudent.GroupId
                                                where GroupStudent.Status=3) as stid
                                                join Student
                                                on Student.Id=stid.StudentId", con);
                cmd.ExecuteNonQuery();
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                DataTable dt = new DataTable();
                da.Fill(dt);
                dataGridView1.DataSource = dt;
            }
            else if(comboBox1.Text=="Report 4")
            {
                var con = Configuration.getInstance().getConnection();
                SqlCommand cmd = new SqlCommand(@"select al.AdvisorId,Lookup.Value,al.GroupId,al.ObtainMarks,al.TotalMarks,al.Perentage
                                                from
                                                (select ProjectAdvisor.AdvisorId,ProjectAdvisor.AdvisorRole,stid.GroupId,stid.ObtainMarks,stid.TotalMarks,stid.Perentage
                                                from
                                                (select GroupProject.ProjectId,mrk.GroupId,mrk.ObtainMarks,mrk.TotalMarks,mrk.Perentage
                                                from
                                                (select GroupId,sum(ObtainedMarks) as ObtainMarks,SUM(Evaluation.TotalMarks) as TotalMarks,concat(cast(cast(cast(sum(ObtainedMarks) as float)/cast(SUM(Evaluation.TotalMarks) as float)*100 as float) as varchar),'%')  as Perentage
                                                from GroupEvaluation,Evaluation
                                                where GroupEvaluation.EvaluationId=Evaluation.Id
                                                group by GroupId) as mrk
                                                join GroupProject
                                                on mrk.GroupId=GroupProject.GroupId) as stid
                                                join ProjectAdvisor
                                                on ProjectAdvisor.ProjectId=stid.ProjectId) al
                                                join Lookup
                                                on Lookup.Id=al.AdvisorRole
                                                where Lookup.Category='ADVISOR_ROLE'", con);
                cmd.ExecuteNonQuery();
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                DataTable dt = new DataTable();
                da.Fill(dt);
                dataGridView1.DataSource = dt;
            }
            else if(comboBox1.Text=="Report 5")
            {
                var con = Configuration.getInstance().getConnection();
                SqlCommand cmd = new SqlCommand(@"select Student.RegistrationNo,CONCAT(Person.FirstName + ' '  , Person.LastName) as Name,Project.Title AS ProjectTitle
                                                  from Student,Person,GroupStudent,GroupProject,Project
                                                  where Student.Id=Person.Id and GroupStudent.StudentId=Student.Id and GroupStudent.Status=3 and GroupProject.GroupId=GroupStudent.GroupId and Project.Id=GroupProject.ProjectId ", con);
                cmd.ExecuteNonQuery();
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                DataTable dt = new DataTable();
                da.Fill(dt);
                dataGridView1.DataSource = dt;
            }
        }
    }
}
