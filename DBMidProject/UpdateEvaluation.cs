﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.SqlClient;
using System.Windows.Forms;
using System.Text.RegularExpressions;

namespace DBMidProject
{
    public partial class UpdateEvaluation : Form
    {
        Regex ex = new Regex("^[0-9]*$");

        public UpdateEvaluation()
        {
            InitializeComponent();
            var con = Configuration.getInstance().getConnection();
            SqlCommand cmd3 = new SqlCommand("Select ID from Evaluation ", con);
            SqlDataAdapter da3 = new SqlDataAdapter(cmd3);
            DataSet ds3 = new DataSet();
            da3.Fill(ds3, "Evaluation");
            comboBox1.ValueMember = "Id";
            comboBox1.DataSource = ds3.Tables["Evaluation"];
            comboBox1.SelectedIndex = -1;
        }

        private void UpdateEvaluation_Load(object sender, EventArgs e)
        {

        }

        private void comboBox1_SelectedIndexChanged(object sender, EventArgs e)
        {
            var con = Configuration.getInstance().getConnection();
            SqlCommand cmd = new SqlCommand("Select * from Evaluation WHERE ID=@ID", con);
            cmd.Parameters.AddWithValue("@ID", comboBox1.Text);
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            DataTable dt = new DataTable();
            da.Fill(dt);
            dataGridView1.DataSource = dt;
            dataGridView1.Columns["ID"].ReadOnly = true;
            dataGridView1.Columns["TotalMarks"].ReadOnly = true;

        }

        private void button2_Click(object sender, EventArgs e)
        {
            if (dataGridView1.Rows.Count > 1)
            {
                if (ex.IsMatch(dataGridView1.Rows[dataGridView1.CurrentRow.Index].Cells[2].FormattedValue.ToString()) && ex.IsMatch(dataGridView1.Rows[dataGridView1.CurrentRow.Index].Cells[3].FormattedValue.ToString()))
                {
                    var con = Configuration.getInstance().getConnection();
                    SqlCommand cmd = new SqlCommand("UPDATE Evaluation SET Name=@Name,TotalMarks=@TotalMarks,TotalWeightage=@TotalWeightage WHERE ID=@ID", con);
                    cmd.Parameters.AddWithValue("@Name", dataGridView1.Rows[dataGridView1.CurrentRow.Index].Cells[1].FormattedValue.ToString());
                    cmd.Parameters.AddWithValue("@TotalMarks", dataGridView1.Rows[dataGridView1.CurrentRow.Index].Cells[2].FormattedValue.ToString());
                    cmd.Parameters.AddWithValue("@TotalWeightage", dataGridView1.Rows[dataGridView1.CurrentRow.Index].Cells[3].FormattedValue.ToString());
                    cmd.Parameters.AddWithValue("@ID", dataGridView1.Rows[dataGridView1.CurrentRow.Index].Cells[0].FormattedValue.ToString());

                    cmd.ExecuteNonQuery();
                    MessageBox.Show("Successfully Updated");
                }
                else
                {
                    MessageBox.Show("Please correct the format of total marks and weightage");
                }
            }
            else
            {
                MessageBox.Show("Please select data");
            }
        }

        private void button3_Click(object sender, EventArgs e)
        {
            if (dataGridView1.Rows.Count > 1)
            {
                var con = Configuration.getInstance().getConnection();

                SqlCommand cmd2 = new SqlCommand("DELETE FROM GroupEvaluation WHERE EvaluationID=@ID", con);
                cmd2.Parameters.AddWithValue("@ID", dataGridView1.Rows[dataGridView1.CurrentRow.Index].Cells[0].FormattedValue.ToString());
                cmd2.ExecuteNonQuery();

                SqlCommand cmd = new SqlCommand("DELETE FROM Evaluation WHERE ID=@ID", con);
                cmd.Parameters.AddWithValue("@ID", dataGridView1.Rows[dataGridView1.CurrentRow.Index].Cells[0].FormattedValue.ToString());
                cmd.ExecuteNonQuery();
                MessageBox.Show("Successfully Deleted");
            }
            else
            {
                MessageBox.Show("Please select data");
            }
        }

        private void dataGridView1_DataError(object sender, DataGridViewDataErrorEventArgs e)
        {
            MessageBox.Show("Please correct the format of the update data");
        }
    }
}
