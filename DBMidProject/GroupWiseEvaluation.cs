﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.SqlClient;
using System.Windows.Forms;
using System.Text.RegularExpressions;

namespace DBMidProject
{
    public partial class GroupWiseEvaluation : Form
    {
        public GroupWiseEvaluation()
        {
            InitializeComponent();
            var con = Configuration.getInstance().getConnection();
            SqlCommand cmd3 = new SqlCommand("Select ID from [Group] where ID in (select GroupId from GroupProject)", con);
            SqlDataAdapter da3 = new SqlDataAdapter(cmd3);
            DataSet ds3 = new DataSet();
            da3.Fill(ds3, "Group");
            comboBox1.ValueMember = "Id";
            comboBox1.DataSource = ds3.Tables["Group"];
            comboBox1.SelectedIndex = -1;


        }

        private void GroupWiseEvaluation_Load(object sender, EventArgs e)
        {

        }

        private void comboBox1_SelectedIndexChanged(object sender, EventArgs e)
        {
            var con = Configuration.getInstance().getConnection();
            SqlCommand cmd4 = new SqlCommand("Select ID from [Evaluation] WHERE ID NOT IN (SELECT EvaluationID FROM GroupEvaluation WHERE GroupID=@GID)", con);
            cmd4.Parameters.AddWithValue("@GID", comboBox1.Text);
            SqlDataAdapter da4 = new SqlDataAdapter(cmd4);
            DataSet ds4 = new DataSet();
            da4.Fill(ds4, "Evaluation");
            comboBox2.ValueMember = "Id";
            comboBox2.DataSource = ds4.Tables["Evaluation"];
            comboBox2.SelectedIndex = -1;
        }
        Regex ex = new Regex("^[0-9]*$");

        private void button1_Click(object sender, EventArgs e)
        {
            if(comboBox1.Text!="" && comboBox2.Text!="" && obtainMarks.Text!=null)
            {
                if (ex.IsMatch(obtainMarks.Text))
                {
                    var con = Configuration.getInstance().getConnection();

                    SqlCommand cmd2 = new SqlCommand("SELECT TotalMarks from Evaluation where Id=@id", con);
                    cmd2.Parameters.AddWithValue("@id", comboBox2.Text);
                    cmd2.ExecuteNonQuery();
                    if (int.Parse(obtainMarks.Text)<=int.Parse(cmd2.ExecuteScalar().ToString()))
                    {
                        SqlCommand cmd = new SqlCommand("Insert into GroupEvaluation values (@GroupID, @EvaluationID,@ObtainMarks,@Date)", con);
                        cmd.Parameters.AddWithValue("@GroupID", comboBox1.Text);
                        cmd.Parameters.AddWithValue("@EvaluationID", comboBox2.Text);
                        cmd.Parameters.AddWithValue("@ObtainMarks", obtainMarks.Text);
                        cmd.Parameters.AddWithValue("@Date", DateTime.Now);
                        cmd.ExecuteNonQuery();
                        MessageBox.Show("Evaluation saved successfully");
                    }
                    else
                    {
                        MessageBox.Show("Obtain marks can not be greater than total marks");
                    }
                }
                else
                {
                    MessageBox.Show("Please correct the format of obtain marks");
                }
            }
            else
            {
                MessageBox.Show("Please inser data in the input fields");
            }
        }
    }
}
